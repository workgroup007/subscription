import { ResponseBuilder } from '../../helpers/responseBuilder';
import Currency from '../../models/currency';
import Plans from '../../models/plans';
import PlanPrice from '../../models/plan-price';
import UserPlans from '../../models/user-plans';
import User from '../../models/users';

export class SubscriptionUtils{
    public async getPlans(currency) : Promise<ResponseBuilder>{
        const planData :any = await Plans.findAll({attributes:['name','type','description','is_active','plan_features'],
            include:[{
                model:PlanPrice,
                attributes:['price'],
                required:true,
                include:[{
                    model:Currency,
                    attributes:['name','symbol'],
                    where:{name:currency},
                    required:true
                }]
            }],
        });
        if(planData){
            return ResponseBuilder.data(planData);        
        }
        else{
            return ResponseBuilder.errorMessage('Sorry No Plans available.');        
        }
    }
    public async getHistories(userId) : Promise<ResponseBuilder>{
        const data = await UserPlans.findAll({attributes:['plan_name','expire_date','total_employee','remaining_employee_limit','total_price','referrel_code','total_price',['createdAt','Plan_Start_date']],
            include:[{
                model:User,
                attributes:[['first_name','referrel_first_name'],['last_name','referrel_last_name'],'email']
            },{
                model:Plans,
                attributes:['name','type'],
                include:[{
                    model:PlanPrice,
                    attributes:['price'],
                    include:[{
                        model:Currency,
                        attributes:['name','symbol']
                    }]
                }]
            }],
            where:{
                user_id:userId
            }
        });
        // console.log(data);
        return ResponseBuilder.data(data);
    }
}