import {Request,Response} from 'express';
import {SubscriptionUtils} from './subscription-utils';

export class SubscriptionController{
    private subscriptionUtils : SubscriptionUtils = new SubscriptionUtils();
    
    public getPlans = async(req:Request,res:Response) => {
        const currency = req.query.currency;
        console.log(currency);
        const result = await this.subscriptionUtils.getPlans(currency);
        return res.status(result.code).json(result.result);
    };

    public histories = async(req:Request,res:Response) => {
        const {user_id} = req.query;
        const result = await this.subscriptionUtils.getHistories(user_id);
        return res.status(result.code).json(result.result);
    };
}