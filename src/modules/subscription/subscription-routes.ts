import { Router } from 'express';
import { SubscriptionController } from './subscription-controller';
import { SubscriptionMiddleware } from './subscription-middleware';
const router:Router = Router();
const subscriptionController = new SubscriptionController();
const subscriptionMiddleware = new SubscriptionMiddleware();

router.post('/buy',subscriptionMiddleware.createRazorpayOrder);
router.get('/plans',subscriptionController.getPlans);
router.get('/histories',subscriptionController.histories);

export const subscriptionRoute:Router = router;
