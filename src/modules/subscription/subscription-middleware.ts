import { Razorpay } from '../../helpers/razorpay';
import { Constants } from '../../helpers/config/constants';
import { Response } from 'express';

export class SubscriptionMiddleware {
    private razorpay: Razorpay = new Razorpay();

    public createRazorpayOrder = async (req: any, res: Response, next: () => void) => {
        const amount: number = +req.body.paidAmount;
        const id = req.body.id || 1;
        const ownerFullName = req.body.ownerFullName || 'name';
        const receipt = `${ownerFullName}(${id}) ${Constants.CREATE_ORDER}`;
        const details: any = await this.razorpay.createOrder(amount, receipt);
        if (details.error) {
            return res.status(Constants.FAIL_CODE).json({
                error: details.error.description
            });
        } else {
            req.body.orderId = details.id;
            next();
        }
    };
}