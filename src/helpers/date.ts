import * as moment from 'moment-timezone';
import { Constants } from './config/constants';

export class Dates {
    public getDateDifference(
        endDate: any,
        startDate: any = moment(),
        diffIn: string = Constants.DATE_DIFFERENCE_IN.DAYS
    ) {
        const expireDate: any = moment(endDate);
        return expireDate.diff(startDate, diffIn);
    }

    public addDays(date: Date, days: number, format: string = null) {
        const newDate = moment(date).add(days, 'days');
        newDate.set({ second: 0, millisecond: 0 });

        if (format) {
            return newDate.format(format);
        } else {
            return newDate.format();
        }
    }

    public nextDueDate(
        recurringFrequency: string,
        dueDate: any,
        recurringDate: any
    ) {
        let updatedDueDate: any;

        switch (recurringFrequency) {
        case 'Weekly':
            updatedDueDate = this.addDays(
                dueDate,
                7,
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        case 'Monthly':
            // updatedDueDate = this.addMonths(dueDate, 1, Constants.DATE_TIME_FORMAT);
            updatedDueDate = moment(dueDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        case 'Yearly':
            // updatedDueDate = this.addYears(dueDate, 1, Constants.DATE_TIME_FORMAT);
            updatedDueDate = moment(dueDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        case 'Quarterly':
            // updatedDueDate = this.addQuarters(dueDate, 1, Constants.DATE_TIME_FORMAT);
            updatedDueDate = moment(dueDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        case 'HalfYearly':
            // updatedDueDate = this.addMonths(dueDate, 6, Constants.DATE_TIME_FORMAT);
            updatedDueDate = moment(dueDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        case 'Custom':
            updatedDueDate = this.addDays(
                dueDate,
                +recurringDate,
                Constants.DATE_TIME_24Hours_FORMAT
            );
            break;

        default:
            break;
        }

        return updatedDueDate;
    }
}
