import * as RazorpayInstance from 'razorpay';
import { Constants } from './config/constants';
import * as _ from 'lodash';
import { Log } from './logger';

export class Razorpay {
    private logger: any = Log.getLogger();

    private instance = new RazorpayInstance({
        key_id: process.env.RAZORPAYID,
        key_secret: process.env.RAZORPAYSECRET,
    });

    public createOrder = async (amount: number, receipt: string, currency: string = Constants.RAZORPAY.DEFAULT_CURRENCY) => {
        amount = _.round(_.multiply(amount, 100));
        const options = {
            amount,
            receipt,
            currency,
        };
        const result = await new Promise((resolve, reject) => {
            this.instance.orders.create(options, (err, res) => {
                if (err) {
                    this.logger.error(`Error in order creation at ${new Date()}: ${err}`);
                    resolve(err);
                } else {
                    this.logger.info(`Order Created at ${new Date()}: ${res}`);
                    resolve(res);
                }
            });
        });
        return result;
    };
}