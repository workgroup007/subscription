import * as moment from 'moment';
import { Constants } from './config/constants';
import { Dates } from './date';
import * as CryptoJS from 'crypto-js';




export class Utils {

    public static dateUtil = new Dates();

    public static getDateForTask = (
        frequency: string,
        recurringDate: any = null,
        dateFor: string = Constants.RECURRING_DATE_FOR.TASK
    ) => {
        let newDate: any = moment();
        let addType: any = Constants.DATE_DIFFERENCE_IN.DAYS;
        const month = moment().format(Constants.MOMENT_DEFAULT.MM);
        const year = moment().format(Constants.MOMENT_DEFAULT.YYYY);
        let currentQuarter: number;
        let diffOfQuarter: number;
        switch (frequency) {
        // Custom DATE
        case Constants.RECURRING_FREQUENCY.Custom: {
            newDate = moment().add(
                Constants.DATE_DIFFERENCE_IN.DAYS,
                recurringDate
            );
            addType = Constants.DATE_DIFFERENCE_IN.DAYS;
            return Utils.getTaskDate(newDate, addType, dateFor);
        }

        // WEEKLY DATE
        case Constants.RECURRING_FREQUENCY.Weekly: {
            newDate = moment().weekday(+recurringDate);
            addType = Constants.DATE_DIFFERENCE_IN.WEEKS;
            return Utils.getTaskDate(
                newDate,
                addType,
                dateFor,
                currentQuarter,
                diffOfQuarter
            );
        }

        // MONTHLY DATE
        case Constants.RECURRING_FREQUENCY.Monthly: {
            if (recurringDate === Constants.RECURRING_VALUE_LAST) {
                newDate = moment().endOf(Constants.MOMENT_DEFAULT.MONTH);
            } else {
                const date = `${month}-${recurringDate}-${year}`;
                newDate = moment(date);
            }
            addType = Constants.DATE_DIFFERENCE_IN.MONTHS;
            return Utils.getTaskDate(newDate, addType, dateFor);
        }

        // HALFYEARLY DATE
        case Constants.RECURRING_FREQUENCY.HalfYearly: {
            return moment(recurringDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
        }

        // QUARTERLY DATE
        case Constants.RECURRING_FREQUENCY.Quarterly: {
            return moment(recurringDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
        }

        // YEARLY DATE
        case Constants.RECURRING_FREQUENCY.Yearly: {
            return moment(recurringDate).format(
                Constants.DATE_TIME_24Hours_FORMAT
            );
        }
        }
    };

    public static getTaskDate = (newDate: any, addType: any, dateFor: string, currentQuarter: any = null, diffOfQuarter: any = null) => {
        const diffRoot = dateFor === Constants.RECURRING_DATE_FOR.SERVICE ? 0 : 1;
        if (Utils.dateUtil.getDateDifference(newDate) < +diffRoot) { // IF DATE IS PASS
            if (currentQuarter && diffOfQuarter) {
                const startOfCurrentQuarter: any = moment().quarter(currentQuarter + 1).startOf(Constants.MOMENT_DEFAULT.QUARTER);
                newDate = moment(Utils.dateUtil.addDays(startOfCurrentQuarter, diffOfQuarter));
            } else {
                if (addType === Constants.DATE_DIFFERENCE_IN.SIXMONTH) {
                    newDate = newDate.add(6, Constants.DATE_DIFFERENCE_IN.MONTHS);
                } else if (addType == Constants.DATE_DIFFERENCE_IN.DAYS) {
                    // eslint-disable-next-line no-self-assign
                    newDate = newDate;
                } else {
                    newDate = newDate.add(1, addType);
                }
            }
        }
        return newDate.format(Constants.DATE_TIME_FORMAT);
    };

    public static getSkipLimit = (page: number, recordsPerPage: number = null) => {
        let skip = 0;
        const limit = recordsPerPage ? recordsPerPage : Constants.RECORDS_PER_PAGE; // for paginate records
        if (page) {
            skip = (page - 1) * limit;
        }
        return { limit, skip };
    };

    public static idsDecryption = (id : any) => {
        id = id.replaceAll(' ', '+');
        id = +(CryptoJS.AES.decrypt(id.trim(), '').toString(CryptoJS.enc.Utf8));
        return id;
    }; 
}
