export class Tables {
    public static readonly USER = 'users_details';
    public static readonly CURRENCY = 'currencies';
    public static readonly PLAN_PRICE = 'plan_prices';
    public static readonly PLANS = 'plans';
    public static readonly USER_PLAN = 'user_plans';
    public static readonly TRANSACTIONS = 'transactions';
}
