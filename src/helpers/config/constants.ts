export class Constants {
    public static readonly UNAUTHORIZED_CODE = 401;
    public static readonly NOT_FOUND_CODE = 404;
    public static readonly SUCCESS_CODE = 200;
    public static readonly INTERNAL_SERVER_ERROR_CODE = 500;
    public static readonly FAIL_CODE = 400;
    public static readonly FORBIDDEN_CODE = 403;
    public static readonly TIMEZONE = 'Asia/Kolkata';
    public static readonly SUCCESS = 'SUCCESS';
    public static readonly ERROR = 'ERROR';
    public static readonly BAD_DATA = 'BAD_DATA';
    public static readonly BACKEND_API_FAILURE = 'BACKEND_API_FAILURE';
    public static readonly CODE = 'CODE';
    public static readonly APPROVED = 'APPROVED';
    public static readonly INVALID_REQUEST = 'INVALID_REQUEST';
    public static readonly IMAGE_MIMES = [
        'image/jpeg',
        'image/jpg',
        'image/png',
    ];
    public static readonly IMAGE_TYPES = ['.jpeg', '.jpg', '.png'];
    public static readonly UPLOAD_FOLDER = 'uploads';
    public static readonly FILE_TYPES = {
        IMAGE: 'img',
        DOCUMENT: 'doc',
        VIDEO: 'vid',
    };
    public static readonly THUMB_TYPES = {
        SMALL: 'small',
        MEDIUM: 'medium',
        LARGE: 'large',
    };
    public static readonly VIDEO_MIMES = ['video/mp4', 'video/quicktime'];
    public static readonly IMAGE_TYPE_UNDEFINED = 'GENERAL';
    public static readonly DATE_TIME_FORMAT = 'YYYY-MM-DD hh:mm:ss';
    public static readonly DATE_TIME_24Hours_FORMAT = 'YYYY-MM-DD HH:mm:ss';
    public static readonly DATE_FORMAT = 'YYYY-MM-DD';
    public static readonly RECORDS_PER_PAGE = 15;
    public static readonly PASSWORD_HASH = 12;
    public static readonly RANDOM_ID_COUNT = 3;
    public static readonly HEX = 'hex';
    public static readonly MD5 = 'md5';

    public static readonly RECURRING_DATE_FOR: any = {
        SERVICE: 'service',
        TASK: 'task',
    };

    public static readonly DATE_DIFFERENCE_IN = {
        DAYS: 'days',
        MONTHS: 'months',
        SIXMONTH: '6months',
        YEARS: 'years',
        WEEKS: 'weeks',
        MINITUES: 'minitues',
        SECONDS: 'seconds',
    };

    public static readonly MOMENT_DEFAULT: any = {
        MM: 'MM',
        YYYY: 'YYYY',
        MONTH: 'month',
        QUARTER: 'quarter',
    };

    public static readonly RECURRING_FREQUENCY = {
        Custom: 'Custom',
        Weekly: 'Weekly',
        Monthly: 'Monthly',
        Yearly: 'Yearly',
        Quarterly: 'Quarterly',
        HalfYearly: 'HalfYearly',
    };

    public static readonly RECURRING_VALUE_LAST = 'last';

    public static readonly RAZORPAY = {
        DEFAULT_CURRENCY: 'USD'
    };
    public static readonly CREATE_ORDER = 'create a order';
    public static readonly DEFULT_GST_PER = 18;
    public static readonly OFFLINEORDERINAPP = 'TSKOPADINAPP';
    public static readonly OFFLINEORDER = 'TSKOPAD';

}
