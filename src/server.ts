import * as compression from 'compression';
import * as  express from 'express';
import * as  dotenv from 'dotenv';
import * as swaggerUI from 'swagger-ui-express';
import { swaggerSpec } from './swagger';
import helmet from 'helmet';
import * as methodOverride from 'method-override'; 
import * as morgan from 'morgan'; 
import { Log } from './helpers/logger';
import db from './models/index';
import { Routes } from './routes';
import * as l10n from 'jm-ez-l10n';

dotenv.config();
db.sync();

export class App {
    protected app: express.Application;
    private logger = Log.getLogger();
    constructor() {
        const NODE_ENV: any = process.env.NODE_ENV;
        this.app = express();
        this.app.use(helmet());
        this.app.all('/*', (req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Request-Headers', '*');
            res.header('Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept,Access-Control-Allow-Headers, Authorization');
            res.header('Access-Control-Allow-Methods', 'GET, POST, PUT , DELETE');
            if (req.method === 'OPTIONS') {
                res.writeHead(200);
                res.end();
            } else {
                next();
            }
        });

        // swagger
        this.app.use(
            '/swagger',
            swaggerUI.serve,
            swaggerUI.setup(swaggerSpec)
        );

        this.app.use(morgan('dev')); 
        this.app.use(compression());
        l10n.setTranslationsFile('en', 'src/language/translation.en.json');
        this.app.use(l10n.enableL10NExpress);
        this.app.use(express.json({ limit: '50mb' }));
        this.app.use(express.urlencoded({ extended: true })); 
        this.app.use(express.json(), (error: any, req: any, res: any, next: any) => {
            if (error) {
                return res.status(400).json({ error: req.t('ERR_GENRIC_SYNTAX') });
            }
            next();
        });
        this.app.use(express.json({ type: 'application/vnd.api+json' })); 
        this.app.use(methodOverride());
        const routes = new Routes(NODE_ENV);
        this.app.use('/', routes.path());
        this.app.listen(`${process.env.PORT}`, () => {
            this.logger.info(`The server is running in port localhost: ${process.env.PORT}`);
            this.app.use((err: any, req: any, res: any, next: () => void) => {
                if (err) {
                    res.status(500).json({ error: req.t('ERR_INTERNAL_SERVER') });
                    return;
                }
                next();
            });
        });
    }
}
