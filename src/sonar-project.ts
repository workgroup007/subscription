import * as sonarqubeScanner from 'sonarqube-scanner';
import * as dotenv from 'dotenv';
dotenv.config();

sonarqubeScanner(
    {
        serverUrl: process.env.SONAR_URL,
        options: {
            'sonar.sources': '.',
            'sonar.inclusions': 'src/**', // Entry point of your code
            'sonar.projectName': 'taskopad-micro-services-organization-service',
            'sonar.projectVersion': '1',
            'sonar.login': process.env.SONAR_ADMIN,
            'sonar.password': process.env.SONAR_PASS,
        },
    },
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    () => {}
);
