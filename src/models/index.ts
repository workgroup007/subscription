import sequelize from '../connection';
import Currency from './currency';
import PlanPrice from './plan-price';
import Plans from './plans';
import UserPlans from './user-plans';
import User from './users';

User.build();
Currency.build();
PlanPrice.build();
Plans.build();
UserPlans.build();

export default sequelize;