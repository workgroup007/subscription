import { BIGINT, BOOLEAN, DATE, DECIMAL, ENUM, INTEGER, STRING, NOW, FLOAT } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';

export class UserSchema {
    public static users = {
        user_id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        old_id:{
            type:INTEGER
        },
        email: {
            type: STRING,
            allowNull: false,
            unique: true,
        },
        email_verification_token: {
            type: STRING
        },
        password: {
            type: STRING,
            allowNull: false
        },
        referral_code: {
            type: INTEGER
        },
        first_name: {
            type: STRING,
            allowNull: false
        },
        last_name: {
            type: STRING,
            allowNull: false
        },
        isd_code: {
            type: STRING
        },
        mobile_number: {
            type: BIGINT,
            allowNull: false
        },
        status: {
            type: ENUM('active','deactive','deleted'),
            allowNull: true
        },
        user_theme_preference: {
            type: BOOLEAN
        },
        date_of_birth: {
            type: FLOAT
        },
        gender: {
            type: ENUM('male','female'),
            allowNull: true
        },
        reset_password_token: {
            type: STRING
        },
        reset_password_token_expiry: {
            type: FLOAT
        },
        email_verification_token_expiry: {
            type: FLOAT
        },
        wallet: {
            type: DECIMAL
        },
        address: {
            type: STRING
        },
        designation: {
            type: STRING,
        },
        createdAt: {
            type: DATE,
            defaultValue: NOW
        },
        updatedAt: {
            type: DATE,
            defaultValue: NOW
        },
        profile_image:{
            type:STRING
        },
        last_update_password:{
            type:FLOAT
        },
    };
}

const User = sequelize.define(Tables.USER, UserSchema.users);
export default User;