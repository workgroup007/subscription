import { INTEGER, STRING, DATE, ENUM } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';
import PlanPrice from './plan-price';
import Plans from './plans';
import User from './users';


export class UserPlanSchema {
    public static userPlan = {
        user_plan_id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id:{
            type:INTEGER,
            allowNull:false
        },
        plan_price_id: {
            type: INTEGER,
        },
        expire_date: {
            type: DATE,
            allowNull:false
        },
        plan_name: {
            type: STRING,
            allowNull:false
        },
        price_per_user: {
            type: STRING,
        },
        currrent_active_plan_id: {
            type:INTEGER
        },
        total_employee: {
            type: STRING,
        },
        remaining_employee_limit:{
            type:STRING
        },
        total_price: {
            type: STRING,
        },
        description: {
            type: STRING
        },
        is_process_completed: {
            type:ENUM('0','1')
        },
        referrel_code:{
            type:STRING
        },
        referrel_by:{
            type:INTEGER
        },
        last_transaction_id:{
            type:STRING
        },
        subscription_type:{
            type:STRING,
        },
        order_id:{
            type:STRING
        },
        storage_capacity:{
            type:INTEGER
        },
        discount_amount:{
            type:STRING
        },
        paid_amount:{
            type:STRING
        },
        gst:{
            type:STRING
        },
        is_in_app:{
            type:ENUM('0','1')
        },
        in_app_diff_amount:{
            type:STRING
        }
    };
}


const UserPlans = sequelize.define(Tables.USER_PLAN, UserPlanSchema.userPlan);

Plans.hasMany(UserPlans,{foreignKey:'currrent_active_plan_id'});        //relation with Plans
UserPlans.belongsTo(Plans,{foreignKey:'currrent_active_plan_id'});


PlanPrice.hasMany(UserPlans,{foreignKey:'plan_price_id'});          //relation with PlanPrice
UserPlans.belongsTo(PlanPrice,{foreignKey:'plan_price_id'});


User.hasMany(UserPlans,{foreignKey:'referrel_by'});
UserPlans.belongsTo(User,{foreignKey:'referrel_by'});


export default UserPlans;