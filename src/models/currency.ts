import { INTEGER, STRING } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';

export class CurrencySchema {
    public static currency = {
        id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: STRING,
            allowNull: false
        },
        symbol: {
            type:STRING,
            allowNull: false
        }
    };
}

const Currency = sequelize.define(Tables.CURRENCY, CurrencySchema.currency);

export default Currency;