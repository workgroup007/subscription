import { INTEGER, FLOAT } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';
import Currency from './currency';



export class PlanPriceSchema {
    public static PlanPrice = {
        plan_price_id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        currency_id: {
            type:INTEGER,
            allownull:false
        },
        price:{
            type:FLOAT,
            allownull:false
        }
    };
}

const PlanPrice = sequelize.define(Tables.PLAN_PRICE, PlanPriceSchema.PlanPrice);

Currency.hasMany(PlanPrice,{foreignKey:'currency_id'});
PlanPrice.belongsTo(Currency,{foreignKey:'currency_id'});

export default PlanPrice;