import { INTEGER, STRING, JSONB } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';
import Currency from './currency';
import PlanPrice from './plan-price';

export class PlanSchema {
    public static plan = {
        plan_id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        free_employee_limits: {
            type: INTEGER,
        },
        free_storage_limits: {
            type: INTEGER
        },
        free_trial_days: {
            type: INTEGER
        },
        name: {
            type: STRING,
            allowNull: false
        },
        type: {
            type: STRING,
            allowNull: false
        },
        plan_price_id: {
            type: INTEGER,
            allowNull: false
        },
        description: {
            type: STRING
        },
        is_active: {
            type: INTEGER,
        },
        attachment_id: {
            type: INTEGER,
        },
        plan_features:{
            type:JSONB
        }
    };
}


const Plans = sequelize.define(Tables.PLANS, PlanSchema.plan);

Currency.hasMany(Plans,{foreignKey:'currency_id'});
Plans.belongsTo(Currency,{foreignKey:'currency_id'});

PlanPrice.hasMany(Plans,{foreignKey:'plan_price_id'});
Plans.belongsTo(PlanPrice,{foreignKey:'plan_price_id'});

export default Plans;