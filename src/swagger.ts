import * as swaggerJSDoc  from 'swagger-jsdoc';
import * as  dotenv from 'dotenv';
dotenv.config();

const option = { 
    definition : {
        openapi : '3.0.3',
        info : {
            title : process.env.SWAGGER_TITLE,
            version : '1.0.0'
        },
        servers : [
            {
                url : process.env.SWAGGER_URL
            }
        ],
    },
    apis:['**/organization-settings-routs.ts']
};
export const swaggerSpec = swaggerJSDoc(option);