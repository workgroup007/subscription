import * as dotenv from 'dotenv';
import { Sequelize } from 'sequelize';
import { Log } from './helpers/logger';

dotenv.config();
const DATABASE: any = process.env.DATABASE;
const DB_USER: any = process.env.DB_USER;
const DB_PASSWORD: any = process.env.DB_PASSWORD;
const DB_PORT: any = process.env.DB_PORT;
const DB_HOST: any = process.env.DB_HOST;
const sequelize = new Sequelize(DATABASE, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        port: DB_PORT,
        dialect: 'postgres',
        logging: true,
        // 0 replication remove replication block from here
        replication: {
            read: [
                // Multiple Replication Goes Here
                {
                    host: DB_HOST,
                    username: DB_USER,
                    password: DB_PASSWORD,
                },

            ],
            write: {},
        },
    },
);

sequelize.authenticate().then(() => {
    Log.getLogger().info('Sequelize Database Connection Established.');
}).catch((err) => {
    Log.getLogger().error('Database Connection Error: ', err);
});

export default sequelize;
