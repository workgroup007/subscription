import * as express from 'express';
import * as l10n from 'jm-ez-l10n';
import { subscriptionRoute } from './modules/subscription/subscription-routes';

export class Routes {
    protected basePath: any;

    constructor(NODE_ENV: string) {
        switch (NODE_ENV) {
        case 'production':
            this.basePath = '/app/dist';
            break;
        case 'development':
            this.basePath = '/app/public';
            break;
        }
    }

    public path() {
        const router = express.Router();

        router.use('/subscription', subscriptionRoute);
        router.all('/*', (req, res) => {
            return res.status(404).json({
                error: l10n.t('ERR_URL_NOT_FOUND'),
            });
        });
        return router;
    }
}
